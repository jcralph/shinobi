 $(document).ready(function() {  
	 
	 var isHidden = true;
	 var menu = $('header .header-nav');
	 /***************** Nav toggle ******************/
	 $('#toggleButton').on('click',function() {
		 
		 if(isHidden){
			 $(menu).css({'visibility':'visible', 'opacity' : '1'}).addClass('animated slideInDown');
				
		 } else {
			 $(menu).css({'visibility':'hidden', 'opacity' : '0'}).removeClass('animated slideInDown');		
		 }
		 
		 isHidden = !isHidden;
	 });
	 
	 $(window).resize(function() {
		 var width = $(window).width();
		 
		 if(width > 990){
			 $(menu).css({'visibility':'visible', 'opacity' : '1'});
		 }
	});
	 
	/***************** Waypoint Animations ****************	**/	 
	$('.wp1').waypoint(function() {
		$('.services').addClass('animated slideInLeft');
		
	}, {
		offset: '100%'
	}); 
	 
	 $('.wp2').waypoint(function() {
		$('.portfolio').addClass('animated slideInRight');
	}, {
		offset: '100%'
	}); 
	 
		 
	/***************** Header BG Scroll ******************/
	$(function() {
		$(window).scroll(function() {
		var scroll = $(window).scrollTop();
		if (scroll >= 20) {
			$('section.navigation').addClass('fixed');	
			if($(window).width() > 991){
				$('header').css({			
					"padding": "20px 0",
				});	
			}
			
		} else {
			$('section.navigation').removeClass('fixed');
			$('header').css({			
				"padding": "40px 0",
			});
		}
		});
	});
	
	
	/***************** Smooth Scrolling ******************/
	$('a.links').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });
	
 });